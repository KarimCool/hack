﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace ConsoleApplication13
{
    class Program
    {
        static void Main(string[] args)
        {
            var m_data = JsonConvert.DeserializeObject<dynamic>(File.ReadAllText("Tuto.json"));
            Console.WriteLine("MOTD : {0}",m_data.MOTD.ToString());
            Console.ReadKey();
        }
    }
}
